<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

class ArticleFilter extends Filter
{
    private const SEARCH_TYPE_VIEWS = 'views';
    private const SEARCH_TYPE_POPULARITY = 'popularity';

    /**
     * Filter the products by the given string.
     *
     * @TODO
     */
    public function search(?string $value = null): Builder
    {
        return $this->builder->whereFuzzy('title', $value)->whereFuzzy('body', $value);
    }

    /**
     * Filter the products by the given status.
     */
    public function categories(?string $value = null): Builder
    {
        $categories = explode(',', $value);

        return $this->builder->whereHas('categories', fn($query) => $query->whereIn('id', $categories));
    }

    /**
     * Filter the products by the given category.
     */
    public function createdBetween(?string $value = null): Builder
    {
        $dates = explode(',', $value);

        return $this->builder->whereBetween('created_at', [$dates[0], [$dates[1]]]);
    }

    /**
     * Sort the products by the given order and field.
     */
    public function sort(array $value = []): ?Builder
    {
        if (isset($value['by'])) {
            $orderBy = isset($value['order']) && $value['order'] === 'asc' ? 'asc' : 'desc';

            if ($value['by'] === self::SEARCH_TYPE_VIEWS) {
                if (isset($value['between'])) {
                    $dates = explode(',', $value['between']);

                    $construction = $this->builder->withCount([
                        'views' => fn($query) => $query->whereBetween('created_at', [$dates[0], [$dates[1]]])
                    ]);
                } else {
                    $construction = $this->builder->withCount('views');
                }

                $builder = $construction->orderBy('views_count', $orderBy);
            }

            if ($value['by'] === self::SEARCH_TYPE_POPULARITY) {
                //
            }
        }

        return $builder ?? $this->builder;
    }
}
