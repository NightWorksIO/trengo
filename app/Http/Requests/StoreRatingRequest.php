<?php

namespace App\Http\Requests;

use App\Rules\OncePerArticle;
use App\Rules\TenTimesADay;
use Illuminate\Foundation\Http\FormRequest;

class StoreRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Add route parameters to validation dataset.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id' => $this->route('id'),
            'ip' => $this->ip(),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'id' => 'required|exists:articles',
            'rating' => 'required|integer|between:1,5',
            'ip' => [new TenTimesADay(), new OncePerArticle()],
        ];
    }
}
