<?php

namespace App\Http\Controllers;

use App\Events\ArticleViewed;
use App\Http\Filters\ArticleFilter;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Resources\Article\ArticleCollection;
use App\Http\Resources\Article\ArticleResource;
use App\Models\Article;
use Illuminate\Http\JsonResponse;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(?ArticleFilter $filter): ArticleCollection
    {
        return new ArticleCollection(Article::filter($filter)->paginate());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreArticleRequest $request): JsonResponse
    {
        $article = Article::create([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        $article->categories()->attach($request->categories);

        return response()->json([
            'status' => ['success']
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): ArticleResource
    {
        $article = Article::findOrFail($id);

        ArticleViewed::dispatch($article);

        return new ArticleResource($article);
    }
}
