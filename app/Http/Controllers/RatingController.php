<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRatingRequest;
use App\Models\Rating;
use Illuminate\Http\JsonResponse;

class RatingController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRatingRequest $request, int $articleId): JsonResponse
    {
        Rating::create([
            'article_id' => $articleId,
            'rating' => $request->rating,
            'ip' => $request->ip(),
        ]);

        return response()->json([
            'status' => ['success']
        ], 201);
    }
}
