<?php

namespace App\Listeners;

use App\Events\ArticleViewed;
use App\Models\View;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class LogArticleView implements ShouldQueue
{
    /**
     * Request object.
     */
    protected Request $request;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     */
    public function handle(ArticleViewed $event): void
    {
        View::create([
            'article_id' => $event->article->id,
            'ip' => $this->request->ip(),
        ]);
    }
}
