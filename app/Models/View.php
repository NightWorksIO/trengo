<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    use HasFactory;

    /**
     * Fillable fields for mass assignment.
     *
     * @var string[]
     */
    protected $fillable = [
        'article_id',
        'ip',
    ];

    /**
     * Overwrite default timestamp settings
     */
    const CREATED_AT = 'viewed_at';
    const UPDATED_AT = null;
}
