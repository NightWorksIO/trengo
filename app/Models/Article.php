<?php

namespace App\Models;

use App\Concerns\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Article extends Model
{
    use HasFactory, Filterable;

    /**
     * Fillable fields for mass assignment.
     *
     * @var string[]
     */
    protected $fillable = [
        'title',
        'body',
    ];

    /**
     * Create relationship with categories.
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Create relationship with views.
     */
    public function views(): HasMany
    {
        return $this->hasMany(View::class);
    }
}
