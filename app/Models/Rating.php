<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    /**
     * Fillable fields for mass assignment.
     *
     * @var string[]
     */
    protected $fillable = [
        'rating',
        'article_id',
        'ip',
    ];
}
