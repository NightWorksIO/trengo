<?php

namespace App\Rules;

use App\Models\Rating;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class TenTimesADay implements Rule
{
    protected const MAX_RATINGS_PER_DAY = 10;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Rating::where('ip', $value)
                    ->whereDate('created_at', Carbon::today())
                    ->count() <= self::MAX_RATINGS_PER_DAY;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You can only rate 10 articles per 24 hours.';
    }
}
