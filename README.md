# Requirements
- PHP 8.1
- Composer v2
- Docker
- In case of Windows, WSL2 with Ubuntu 20 (make sure you enable the WSL2 integration for your Distro in the Docker settings)

# Installation steps
_In case of Windows, execute these commands inside the Terminal of your Ubuntu distro_

1. Run `composer install`
2. Run `cp .env.example .env` and set the MySQL password
3. Run `./vendor/bin/sail up -d`
4. Run `./vendor/bin/sail artisan migrate`
5. Run `./vendor/bin/sail artisan db:seed` (Ratings & Views can take some time due to the high count)
6. Run `./vendor/bin/sail artisan test` in order for the endpoints test to be executed

# Documentation
Endpoint documentation can be found at the following link:
http://localhost/api/documentation

# Rate limiting
Rate limiting has been set to 10 requests per minute, and uses the default configuration in the RouteServiceProvider.
