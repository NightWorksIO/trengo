<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Article::factory()
                ->count(1000)
                ->hasAttached(
                    Category::factory()->count(3)
                )
                ->create();
    }
}
