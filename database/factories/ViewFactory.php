<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ViewFactory extends Factory
{
    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'article_id' => Article::factory(),
            'ip' => long2ip(rand(0, 4294967295)),
        ];
    }
}
