<?php

namespace Tests\Feature;

use Tests\TestCase;

class ArticleTest extends TestCase
{
    public function test_creating_article()
    {
        $response = $this->postJson(
            '/api/articles',
            [
                'title' => 'This is a test article',
                'body' => 'This is a test article its body',
                'categories' => [1, 2, 3],
            ]
        );

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'status' => ['success'],
            ]);
    }

    public function test_add_rating()
    {
        $response = $this->postJson(
            '/api/articles/1/ratings',
            [
                'rating' => 5,
            ]
        );

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'status' => ['success'],
            ]);
    }

    public function test_add_rating_to_same_article_gives_error()
    {
        $response = $this->postJson(
            '/api/articles/1/ratings',
            [
                'rating' => 5,
            ]
        );

        $response
            ->assertStatus(422)
            ->assertExactJson([
                'message' => 'The given data was invalid.',
                'errors' => ['ip' => ['You can only rate an article once.']],
            ]);
    }

    public function test_get_single_article()
    {
        $response = $this->get('/api/articles/1');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => ['id' => 1],
            ]);
    }
}
